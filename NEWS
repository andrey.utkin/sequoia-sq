                                                              -*- org -*-
#+TITLE: sequoia-sq NEWS – history of user-visible changes
#+STARTUP: content hidestars

* Changes in 0.33.0
** Notable changes
   - The command line interface has been restructured.  Please consult
     the manual pages and review any code and documents using the
     interface.  Notably:

     - `sq import` and `sq export` have been moved to `sq cert`.
     - `sq wot` has been renamed to `sq pki`.
     - `sq link` and `sq certify` have been moved to `sq pki`.
     - `sq lookup, `sq keyserver`, `sq wkd`, and `sq dane` have been
       moved to `sq network`.
     - All commands retrieving certificates from network services are
       now called `fetch`, e.g. `sq network fetch` and `sq network
       dane fetch`.  The command for publishing certs on key servers
       is now called `sq network keyserver publish`.
     - `sq armor`, `sq dearmor`, and `sq packet` have been moved to
       `sq toolbox`.
     - `sq --version` is now `sq version`, and `sq output-versions`
       has been integrated with that command.

   - The manual page generation has been improved, and manual pages
     and shell completions are generated during the build process.  To
     write the assets to a predictable location, set the environment
     variable `ASSET_OUT_DIR` to a suitable location.

* Changes in 0.32.0
** New functionality
   - Support for password-encrypted keys has been improved.  For
     example, a newly generated subkey can be password protected.

   - When encrypting a message with a password, or creating a new
     password-protected key or subkey, or changing passwords on a key,
     sq now prompts you to repeat the password to catch typos.

   - Literal data metadata can now be set using
     `--set-metadata-filename` and `--set-metadata-time`.

   - sq now reads the file

       /etc/crypto-policies/back-ends/sequoia.config

     to configure its cryptographic policy.  The file to load can be
     overridden using the SEQUOIA_CRYPTO_POLICY environment variable.
     For more information on the format, see:

     https://docs.rs/sequoia-policy-config/latest/sequoia_policy_config/#format

   - New subcommand: `sq dane generate`.

     Generates DANE records for the given domain and certificates.  DANE is a
     way to distribute OpenPGP certificates via DNS.

     https://www.rfc-editor.org/rfc/rfc7929.html

   - When querying network resources via `sq keyserver get`, `sq wkd
     get`, or `sq dane get`, multiple queries can be given that are
     executed simultaneously.

   - When querying key servers via `sq keyserver get` or `sq keyserver
     send`, multiple servers are queried simultaneously.

   - There are now four default keyservers:

     - hkps://keys.openpgp.org
     - hkps://mail-api.proton.me
     - hkps://keys.mailvelope.com
     - hkps://keyserver.ubuntu.com

   - New subcommand: `sq lookup`.

     Searches for certificates using all supported network services.

** Notable changes
   - Padding has been disabled to increase compatibility with other
     implementations.

     The padding method we previously used relies on a compressed data
     packet to pad the message.  However, some implementations do not
     gracefully process these padded encryption containers, so until
     we get proper padding support from the next revision of OpenPGP,
     we disable padding.

   - Message notarization has been disabled.

     Message notarization and their semantics are not well defined,
     and there is no consensus on how to do that.

   - When doing a userid, subkey, or third-party certificate
     revocation, with the cert given to --certificate-file containing
     secret key material, we previously emitted a revocation
     certificate containing secret key material.  This has been fixed,
     and tests have been added to ensure secret key material is only
     emitted where we expect it to be.
* Changes in 0.31.0
** New functionality
  - `sq key subkey add` allows to create and add a new subkey to an
    existing certificate.
  - The functionality of `sq-keyring-linter` is now available as
    `sq keyring lint`.
  - The new subcommands `sq key revoke`, `sq key subkey revoke` and
    `sq key userid revoke`, allow writing to a file using the
    `--output` option.
** Notable changes
  - The `--keyring` option is now global and can be specified anywhere
    when calling `sq`.
** Deprecated functionality
  - The `--expires` and `--expires-in` options used in various
    subcommands are deprecated in favor of the unifying `--expiry`.
  - `sq key generate --export FILE` is deprecated in favor of the more
    generic `sq key generate --output FILE`.
  - The `sq revoke certificate` command has been renamed to `sq key
    revoke`.
  - The `sq revoke subkey` command has been renamed to `sq key subkey
    revoke`.
  - The `sq revoke userid` command has been renamed to `sq key userid
    revoke`.
* Changes in 0.30.1
** Notable changes
   - The `crypto-botan` feature now selects Botan's v3 interface.  Use
     the new `crypto-botan2` feature to continue using Botan's v2
     interface.
** Notable fixes
   - Several parser bugs were fixed in sequoia-openpgp 1.16.0 and
     buffered-reader 1.2.0.  These are all low-severity as Rust
     correctly detects the out of bounds access and panics.  Update
     Cargo.lock to make sure we use these versions.
* Changes in 0.30
** New functionality
  - `sq key adopt` now honors `--time`.
  - Add `sq key adopt --expire` to allow setting an adopted key's
    expiration time.
  - Add support for using pEp's certificate store.  A pEp certificate
    store can be used by specifying `sq --pep-cert-store PATH` or
    setting the environment variable `PEP_CERT_STORE`.
* Changes in 0.29
** New functionality
   - `sq` now supports and implicitly uses a certificate store.  By
     default, `sq` uses the standard OpenPGP certificate directory.
     This is located at `$HOME/.local/share/pgp.cert.d` on XDG
     compliant systems.
   - `sq --no-cert-store`: A new switch to disable the use of the
     certificate store.
   - `sq --cert-store`: A new option to use an alternate certificate
     store.  Currently, only OpenPGP certificate directories are
     supported.
   - `sq import`: A new command to import certificates into the
     certificate store.
   - `sq export`: A new command to export certificates from the
     certificate store.
   - `sq encrypt --recipient-cert`: A new option to specify a
     recipient's certificate by fingerprint or key ID, which is then
     looked up in the certificate store.
   - `sq verify --signer-cert`: A new option to specify a signer's
     certificate by fingerprint or key ID, which is then looked up in
     the certificate store.
   - `sq verify` now also implicitly looks for missing certificates in
     the certificate store.  But, unless they are explicitly named
     using `--signer-cert`, they are not considered authenticated and
     the verification will always fail.
   - `sq certify`: If the certificate to certify is a fingerprint or
     Key ID, then the corresponding certificate is looked up in the
     certificate store.
   - Add a global option, `--time`, to set the reference time.  This
     option replaces the various subcommand's `--time` argument as
     well as `sq key generate` and `sq key userid add`'s
     `--creation-time` arguments.
   - Add top-level option, `--trust-root`, to allow the user to
     specify trust roots.
   - Extend `sq encrypt` to allow addressing recipients by User ID
     (`--recipient-userid`) or email address (`--recipient-email`).
     Only User IDs that can be fully authenticated are considered.
   - Extend `sq verify` to verify certificates looked up from the
     certificate store using the web of trust.  If the signature
     includes a Signer's User ID packet, and the binding can be fully
     authenticated, consider the signature to be authenticated.  If
     there is no Signer's User ID packet, consider the signature to be
     authenticated if any binding can fully be authenticated.
   - Add `sq link add`, which uses the local trust root to
     certify the specified bindings.
   - Add `sq link retract`, which retracts certifications made by the
     local trust root on the specified bindings.
   - Add `sq link list`, which lists the links.
   - Add a top-level option, `--keyring`, to allow the user to specify
     additional keyrings to search for certificates.
   - Import web of trust subcommands from sq-wot.  Specifically, add:
     - `sq wot authenticate` to authenticate a binding.
     - `sq wot lookup` to find a certificate with a particular User ID.
     - `sq wot identify` to list authenticated bindings for a
       certificate.
     - `sq wot list` to list authenticated bindings.
     - `sq wot path` to authenticate and lint a path in a web of trust.
   - `sq keyserver get`, `sq wkd get`, and `sq dane get` now import any
     certificates into the certificate store by default instead of
     exporting them on stdout.  It is still possible to export them
     using the `--output` option.
   - When `sq keyserver get` (for verifying key servers), `sq wkd get`,
     or `sq dane get` saves a certificate to the local certificate
     store, `sq` certifies the validated User IDs (all returned User
     IDs in the case of verifying key servers; User IDs that contain
     the looked up email address in the case of WKD and DANE) using a
     local service-specific proxy CA.  If the proxy key doesn't exist,
     it is created, and certified as a minimally trusted CA (trust
     amount 1 of 120) by the local trust root.  The proxy certificates
     can be managed in the usual way using `sq link add` and `sq link
     retract`.
   - Extend `sq inspect` to inspect certificates from the certificate
     store using the `--cert` option.
** Deprecated functionality
   - `sq key generate --creation-time TIME` is deprecated in favor of
     `sq key generate --time TIME`.
   - `sq key user id --creation-time TIME` is deprecated in favor of
     `sq user id --time TIME`.
* Started the NEWS file.
